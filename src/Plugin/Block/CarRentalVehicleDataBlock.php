<?php

/**
 * Defines the Vehicle Data Reference Chart.
 */
class CarRentalVehicleDataBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Vehicle data reference chart'),
      'cache' => DRUPAL_NO_CACHE, // DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('Vehicle Data Reference Chart');
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $output[] = CarRentalService::create()->viewVehicleTypes();
  }
}
