<?php

/**
 * Defines the Car Rental Form Block.
 */
class CarRentalFormBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Car rental calculator form'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

    /**
   * {@inheritdoc}
   */
  public function label() {
    return t('Mileage Reimbursement Calculator');
  }

/**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    module_load_include("inc", "car_rental", "forms/car_rental_form");
    $output[] = drupal_get_form('car_rental_form');
  }
}
