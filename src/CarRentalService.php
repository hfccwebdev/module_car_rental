<?php

/**
 * Defines the Car Rental Service Interface.
 */
interface CarRentalServiceInterface {

  /**
   * Creates an instance of this class.
   */
  public static function create();

  /**
   * Retrieves an array of available vehicle types from settings.
   *
   * @return array
   *   The available vehicle types.
   */
  public function getVehicleTypes();

  /**
   * Display formatted vehicle data reference chart.
   *
   * @return array
   *   A renderable array.
   */
  public function viewVehicleTypes();

  /**
   * Retrieves an array of vehicle options for form.
   *
   * @return string[]
   *   Returns a key/name pair for vehicle types.
   */
  public function getVehicleOptions();

  /**
   * Determine if preapproval is required to rent requested vehicle type.
   *
   * @return bool
   *   Returns TRUE if preapproval is required.
   */
  public function preapprovalRequired($type);

  /**
   * Calculate all results.
   *
   * @param $values
   *   All values passed from form.
   *
   *   Required elements:
   *   - total_miles
   *   - total_days
   *   - vehicle_type
   *   - gas_cost
   *   - mileage_charge
   *
   * @return array
   *   Renderable array, should contain:
   *   - own_car_cost
   *   - rental_price
   *   - vehicle_license_recovery_fee
   *   - wayne_county_stadium_tax
   *   - refueling
   *   - total
   */
  public function calculateResults($values);

}

/**
 * Defines the Car Rental Service
 */
class CarRentalService implements CarRentalServiceInterface {

  /**
   * {@inheritdoc}
   */
  public static function create() {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getVehicleTypes() {
    $types = &drupal_static(__FUNCTION__);
    if (!isset($types)) {
      if ($settings = variable_get("car_rental_vehicle_classes", NULL)) {
        $rows = explode(PHP_EOL, $settings);
        $types = [];
        foreach ($rows as $row) {
          $values = explode('|', $row);
          $values = array_map('trim', $values);
          $types[$values[0]] = [
            'name' => $values[1],
            'example' => $values[2],
            'mpg' => $values[3],
            'rate' => $values[4],
            'needs_auth' => $values[5],
          ];
        }
      }
      else {
        drupal_set_message(t('Rental vehicle types not set. Please configure Car Rental module.'), 'error');
      }
    }
    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function viewVehicleTypes() {
    return [
      '#theme' => 'vehicle_data_reference_chart',
      '#rows' => $this->getVehicleTypes(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getVehicleOptions() {

    $types = $this->getVehicleTypes();

    $options = [];

    foreach($types as $key => $type) {
      $options[$key] = $type['name'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function preapprovalRequired($type) {
    $types = $this->getVehicleTypes();
    return drupal_strtolower($types[$type]['needs_auth']) == 'y';
  }

  /**
   * {@inheritdoc}
   */
  public function calculateResults($values) {

    $vehicle_info = $this->getVehicleTypes()[$values['vehicle_type']];

    $output = [];

    $output['own_car_cost'] = $values['total_miles'] * variable_get('car_rental_mileage_rate', 0);

    $output['rental_price'] = $values['total_days'] * $vehicle_info['rate'] + $values['mileage_charge'];

    $output['vehicle_license_recovery_fee'] = $values['total_days'] * 0.65;

    $output['wayne_county_stadium_tax'] = ($output['rental_price'] + $output['vehicle_license_recovery_fee']) * 0.02;

    $output['refueling'] = ($values['total_miles'] / $vehicle_info['mpg']) * $values['gas_cost'];

    $output['total'] = $output['rental_price'] + $output['vehicle_license_recovery_fee'] + $output['wayne_county_stadium_tax'] + $output['refueling'];

    return [
      '#theme' => 'car_rental_results',
      '#items' => $output,
    ];
  }
}
