<?php

/**
 * @file
 * Contains theme_vehicle_data_reference_chart function for the car_rental module.
 */

/**
 * Implements hook_preprocess_HOOK().
 *
 * Theme preprocess function for theme_vehicle_data_reference_chart().
 *
 * @see template_preprocess_field()
 * @see template_preprocess_hfcc_global_pseudo_field()
 */
function template_preprocess_vehicle_data_reference_chart(&$variables, $hook) {

  $vehicles = $variables['element']['#rows'];

  $content = [];

  $format_row = function ($row) {
    $row['name'] = ['data' => $row['name'], 'class' => ['vehicle-class']];
    $row['example'] = ['data' => $row['example'], 'class' => ['vehicle-example']];
    $row['mpg'] = ['data' => $row['mpg'], 'class' => ['vehicle-mpg']];
    $row['rate'] = ['data' => '$' . number_format($row['rate'], 2), 'class' => ['vehicle-rate']];
    $row['needs_auth'] = ['data' => drupal_strtoupper($row['needs_auth']), 'class' => ['vehicle-approval']];
    return $row;
  };

  $content[] = [
    '#theme' => 'table',
    '#header' => [
      t('Vehicle Class'),
      t('Vehicle Examples'),
      t('MPG'),
      t('Daily Rate'),
      t('Preapproval'),
    ],
    '#rows' => array_map($format_row, $vehicles),
    '#attributes' => ['class' => ['vehicle-data-reference-chart']],
    '#empty' =>t('Could not load vehicle data.'),
  ];

  $variables['content'] = $content;
}

/**
 * Theme function for vehicle_data_reference_chart.
 */
function theme_vehicle_data_reference_chart(&$variables) {
  return render($variables['content']);
}
