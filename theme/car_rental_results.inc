<?php

/**
 * @file
 * Contains theme_car_rental_results function for the car_rental module.
 */

/**
 * Implements hook_preprocess_HOOK().
 *
 * Theme preprocess function for theme_car_rental_results().
 *
 * @see template_preprocess_field()
 * @see template_preprocess_hfcc_global_pseudo_field()
 */
function template_preprocess_car_rental_results(&$variables, $hook) {

  $items = $variables['element']['#items'];
  $content = [
    '#prefix' => '<div class="mileage-calculator-results">',
    '#suffix' => '</div>',
  ];

  $content[] = ['#markup' => t('<h2>Calculated Results</h2>')];
  $content[] = ['#markup' => t('<h3>Own Car Cost</h3>')];

  $content[] = [
    '#field_name' => 'own_car_cost',
    '#type' => 'item',
    '#label' => t('Mileage Reimbursement'),
    '#label_display' => 'inline',
    '#markup' => '$' . number_format($items['own_car_cost'], 2),
    '#theme' => 'hfcc_global_pseudo_field',
  ];

  $content[] = ['#markup' => t('<h3>Rental Car Cost</h3>')];

  $content[] = [
    '#field_name' => 'rental_price',
    '#type' => 'item',
    '#label' => t('Rental Price'),
    '#label_display' => 'inline',
    '#markup' => '$' . number_format($items['rental_price'], 2),
    '#theme' => 'hfcc_global_pseudo_field',
    ];

  $content[] = [
    '#field_name' => 'vehicle_license_recovery_fee',
    '#type' => 'item',
    '#label' => t('Vehicle License Recovery Fee'),
    '#label_display' => 'inline',
    '#markup' => '$' . number_format($items['vehicle_license_recovery_fee'], 2),
    '#theme' => 'hfcc_global_pseudo_field',
  ];

  $content[] = [
    '#field_name' => 'wayne_county_stadium_tax',
    '#type' => 'item',
    '#label' => t('Wayne County Stadium Tax'),
    '#label_display' => 'inline',
    '#markup' => '$' . number_format($items['wayne_county_stadium_tax'], 2),
    '#theme' => 'hfcc_global_pseudo_field',
  ];

  $content[] = [
    '#field_name' => 'car_rental_refueling',
    '#type' => 'item',
    '#label' => t('Refueling'),
    '#label_display' => 'inline',
    '#markup' => '$' . number_format($items['refueling'], 2),
    '#theme' => 'hfcc_global_pseudo_field',
  ];

  $content[] = [
    '#field_name' => 'car_rental_total',
    '#type' => 'item',
    '#label' => t('Total'),
    '#label_display' => 'inline',
    '#markup' => '$' . number_format($items['total'], 2),
    '#theme' => 'hfcc_global_pseudo_field',
  ];

  $variables['content'] = $content;
}

/**
 * Theme function for car_rental_results.
 */
function theme_car_rental_results(&$variables) {
  return render($variables['content']);
}
