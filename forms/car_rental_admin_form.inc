<?php

/**
 * @file
 * Enterprise Car Rental Rate Calculator Admin Form
 */

/**
 * Defines the Admin Form.
 */
function car_rental_admin_form($form, &$form_state) {

  $form['car_rental_mileage_rate'] = [
    '#title' => t('Rental Reimbursement Rate'),
    '#type' => 'textfield',
    '#size' => 15,
    '#default_value' => variable_get('car_rental_mileage_rate', NULL),
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

    $form['car_rental_vehicle_classes'] = [
    '#title' => t('Vehicle Rental Information'),
    '#type' => 'textarea',
    '#rows' => 10,
    '#default_value' => variable_get('car_rental_vehicle_classes', NULL),
    '#required' => TRUE,
  ];
  return system_settings_form($form);
}
