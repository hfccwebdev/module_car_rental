<?php

/**
 * @file
 * Enterprise Car Rental Rate Calculator Form
 */

/**
 * Defines the User Form.
 */
function car_rental_form($form, &$form_state) {

  $car_service = CarRentalService::create();

  $form['total_miles'] = [
    '#title' => t('Total miles to be traveled'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

  $form['total_days'] = [
    '#title' => t('Total days in trip'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

  $form['vehicle_type'] = [
    '#type' => 'select',
    '#title' => t('Vehicle class'),
    '#options' => $car_service->getVehicleOptions(),
    '#default_value' => 'compact',
    '#description' => t('See vehicle reference rates to select a vehicle.'),
  ];

  $form['upgrade_approval'] = [
    '#type' => 'checkbox',
    '#title' => t('Upgrade preapproved'),
    '#description' => t('Preapproval is required for some vehicle classes.'),
  ];

  $form['gas_cost'] = [
    '#title' => t('Cost of gasoline per gallon'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#description' => t('Refer to <a href="https://gasprices.aaa.com/?state=MI" target="_blank">AAA Gas Prices</a> for today\'s AAA Michigan Average.'),
    '#required' => TRUE,
  ];

  $form['mileage_charge'] = [
    '#title' => t('Rental mileage charge, if any'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#default_value' => 0,
    '#required' => TRUE,
  ];

  // Provide a submit button.
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Calculate Rates',
  ];

  $form['reset'] = [
    '#markup' => l(t('Reset form'), current_path(), ['attributes' => ['class' => ['form-reset-button']]])
  ];

  if (!empty($form_state['values']['results'])) {
    $form['results'] = $form_state['values']['results'];
  }

  return $form;
}

/**
 * Form validation.
 */
function car_rental_form_validate($form, &$form_state) {

  // Allow upgraded vehicles with preapproval only.
  $car_service = CarRentalService::create();
  $vehicle_type = $form_state['values']['vehicle_type'];
  $preapproved = $form_state['values']['upgrade_approval'];

  if (!$preapproved && $car_service->preapprovalRequired($vehicle_type)) {
    form_set_error('vehicle_type', t('Preapproval is required to select this vehicle option.'));
  }
}

/**
 * Form submit handler.
 */
function car_rental_form_submit($form, &$form_state) {
  $form_state['values']['results'] = CarRentalService::create()->calculateResults($form_state['values']);
  $form_state['rebuild'] = TRUE;
}
